![Alt text](static/images/png-transparent-postgresql-object-relational-database-oracle-database-freebsd-icon-text-logo-head.png)
Caso Windows:
================

Ubicarnos en la carpeta donde instalamos el archivo.

Si hicimos la instalación por defecto estará dentro de la carpeta de nuestro disco duro en archivos de programa (Program Files x86).

Luego buscamos la carpeta PostgreSQL e ingresamos a ella, seguidamente conseguiremos una carpeta de la versión que instalamos, ingresamos a ella y luego buscamos la carpeta llamada scripts.

Una vez dentro de la carpeta scripts, le damos doble clic al archivo runpsql, y ya estaremos en la consola de comandos de postgres.

Allí ingresaremos los datos que hemos configurado, en el caso de solo haber puesto una clave al momento de la instalación, no ingresar datos en las secciones especificadas a continuación, solo debemos pulsar la tecla enter:

- server [localhost],
- Database [postgres],
- port [5432],
- username [postgres].

Al llegar a la sección de contraseña para usuario postgres solo debemos indicar la contraseña que al momento de la instalación definimos.


Caso Linux:
================

Ingresamos a la terminal y ejecutamos el siguiente comando:

``psql -U nombreUsuario -W -h iphost nombreBaseDeDatos``

Parámetros:

``U es el usuario de la base``

``W mostrará el prompt de solicitud de password``

``h IP del servidor de la base de datos en caso nos conectemos remotamente sino bastaría con poner localhost``

 

Si hicimos la instalación por defecto, solo tenemos el usuario postgres por lo tanto escribiríamos lo siguiente: ``psql –U postgres –W –h localhost``  presionamos enter y colocamos la contraseña que definimos al momento de la instalación.

Operaciones básicas en las Bases de datos y tablas.

Primero que nada veamos la siguiente lista de comandos básicos en postgresql.

- \l ``—> Te muestra las bases de datos existentes.``
- \d ``—> Te muestra las relaciones (tablas, secuencias, etc.) existentes en la base de datos.``
- \d [nombre_tabla] ``—> Para ver la descripción (nombre de columnas, tipo de datos, etc.) de una tabla.``
- \c [nombre_bd] ``—> Para conectarte a otra base de datos.``
- SHOW search_path; ``—> Para ver la ruta de búsqueda actual.``
- SET search_path TO [nombre_esquema]; ``—> Para actualizar la ruta de búsqueda.``
- \q ``—–> Para salir de psql``


### Crear una base de datos. ###

Para crear una base de datos debemos escribir lo siguiente: ``CREATE DATABASE nombreBD;``

> Ejemplo:
>
>``CREATE DATABASE curso_postgresql; ``


### Eliminar Bases de Datos ###

Podemos eliminar una o muchas bases de datos con el comando DROP DATABASE utilizándolo de esta manera: DROP DATABASE nombreDB;

Si deseamos borrar multiples bases de datos podrmos hacer lo siguiente: DROP DATABASE nombreDB1,nombreDB2,…..,nombreDBn;

Lo podemos ver en el siguiente ejemplo: ``DROP DATABASE curso_postgresql;``

>> NOTA: Es importante el uso del parámetro IF EXISTS  a la hora de ejecutar las consultas, ya que cuando intentas borrar una base de datos que no existe ocurrirá un error.

Para evitar este error podemos agregar este parámetro a la sentencia DROP y así hacer que la sentencia no conlleve a errores si existen o no las bases de datos que queremos borrar.

>Ejemplo:
>
>``DROP DATABASE IF EXISTS nombreDB;``

En este caso si la tabla existe la borra y si no existe el sistema te advierte que no existe la tabla, pero la consulta no arroja un error.


### Renombrar una base de datos ###

Para renombrar una base de datos necesitamos alterarla de la siguiente manera: ALTER DATABASE nombreBD RENAME TO nombreNuevoDB;

> Ejemplo:
>
>``ALTER DATABASE curso_postgresql RENAME TO curso_pg;``


### Conectar con Bases de Datos Creadas. ###

Para poder usar o crear tablas dentro de una base de datos, primero debemos seleccionarla o conectarnos con ella usando el comando:

>``\c nombreBD;``
>
> Ejemplo:
>
>``\c curso_pg;``

### Cambiar propietario a base de datos 

Para poder cambiar de propietario a una base de datos, podemos hacerlo con el comando ALTER DATABASE:

> `ALTER DATABASE dbname OWNER TO newowner;`