![Sin titulo](static/images/vpn.png)  

openvpn-install
---------------------

OpenVPN installer for Ubuntu and Arch Linux.

This script will let you setup your own secure VPN server in just a few seconds.

You can also check out wireguard-install, a simple installer for a simpler, safer, faster and more modern VPN protocol.

Usage
---------------------------------

First, get the script and make it executable:

    curl -O https://raw.githubusercontent.com/angristan/openvpn-install/master/openvpn-install.sh
    chmod +x openvpn-install.sh

Then run it:

    ./openvpn-install.sh


Headless install
----------------------------------------

It's also possible to run the script headless, e.g. without waiting for user input, in an automated manner.

Example usage:

    AUTO_INSTALL=y ./openvpn-install.sh

Execute 
---------------------------------------

Copy the VPN configuration files into the client directory `/etc/openvpn/client/` and then your run in the terminal inside the directory

    sudo openvpn --config file.ovpn
