# Amazon Web Services AWS

![Sin titulo](static/images/aws.png) 

- [Concept](#Concept)
- [Launch an Amazon EC2 Instance](#instance)
- [EIP Elastic IP](#elastic)
- [Connect to the Instance](#connect)

Amazon Web Services (abbreviated AWS) is a collection of public cloud computing services (also called web services) 
that together form a cloud computing platform, offered through the Internet by Amazon.com.

<a id="instance">Step 1: Launch an Amazon EC2 Instance</a>
----------------------------------------

#### To launch an EC2 instance

1. Open the Amazon EC2 console by choosing **EC2** under **Compute**.

If you are using the Show All Services view, your screen looks like this:

![Sin titulo](static/images/ec2.png) 

2. From the Amazon EC2 dashboard, choose **Launch Instance**.

![Sin titulo](static/images/launch.png) 

3. The **Choose an Amazon Machine Image (AMI)** page displays a list of basic configurations called Amazon Machine Images 
(AMIs) that serve as templates for your instance. Select the HVM edition of the **Amazon Linux AMI**. Notice that this 
configuration is marked **Free tier eligible**.

![Sin titulo](static/images/choose.png) 

4. On the **Choose an Instance Type** page, choose **t.2micro** as the hardware configuration of your instance and **Next: Configure Instance Details**.

    Note
    >T2 instances, such as **t2.micro**, must be launched into a virtual private cloud (VPC). If you don't have a VPC, you can 
    let the wizard create one for you. For more information, see in [Launching an Instance](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/launching-instance.html).

![Sin titulo](static/images/type.png) 

5. On this screen, you can specify the details or you can just click on **Next: Add Storage** to proceed with the default 
settings. Here, we shall proceed with the default settings. 

![Sin titulo](static/images/details.png) 

6. You can specify the size for the root partition. We have specified Root Partition Size as 30 GBs. Click on 
**Next: Add Tags** to proceed

![Sin titulo](static/images/storage.png) 

7. You can specify Tags (Key:Value) or can skip this step and click on **Next: Configure Security Group**.

![Sin titulo](static/images/tag.png) 

8. If you have an existing Security Group you can select that or can create a new. We shall create a new Security 
Group by just selecting `Create a new Security Group` radio button. Click on "Review and Launch"

You can define open ports and IPs.

Since our server is a webserver=, we will do following things
- Creating a new Security Group
- Naming our SG for easier reference
- Defining protocols which we want enabled on my instance
- Assigning IPs which are allowed to access our instance on the said protocols
- Once, the firewall rules are set- Review and launch

![Sin titulo](static/images/security.png) 

9. Now, review your configuration and click on **Launch**

![Sin titulo](static/images/instance.png) 

10. In the next step you will be asked to create a key pair to login to you an instance. A key pair is a set of public-private keys.

AWS stores the private key in the instance, and you are asked to download the private key. Make sure you download the key and keep it safe and secured; if it is lost you cannot download it again.

- Create a new key pair
- Give a name to your key
- Download and save it in your secured folder

![Sin titulo](static/images/key.png) 

11. Wait for some time until the instance gets created. Click on **View Instances** to check the Instance State and other 
details.

![Sin titulo](static/images/ready.png) 

12. Once the Instance State changes from “pending” to ‘running’ you can connect to the instance.

![Sin titulo](static/images/ins.png) 

# <a id="elastic">EIP Elastic IP</a>

An EIP is a static public IP provided by AWS. It stands for Elastic IP. Normally when you create an instance, it will 
receive a public IP from the AWS's pool automatically. If you stop/reboot your instance, this public IP will 
change- it'dynamic. In order for your application to have a static IP from where you can connect via public networks, 
you can use an EIP.

Step 1) On the left pane of EC2 Dashboard, you can go to 'Elastic IPs' as shown below.

![Sin titulo](static/images/ip.png) 

Step 2) Allocate a Elastic IP Address.

![Sin titulo](static/images/alocate.png) 

Step 3) Allocate this IP to be used in a VPC scope.

![Sin titulo](static/images/alocate1.png) 

Step 4) Now assign this IP to your instance.

![Sin titulo](static/images/asociate.png)
 
Step 5) In the next page,
- Search for your instance and
- Associate the IP to it.

![Sin titulo](static/images/asociate1.png) 

![Sin titulo](static/images/asociate2.png) 

# <a id="connect">Connect to the Instance</a>

Once the Instance is available, select the Instance and Click on  `Connect`. Copy the Example  ssh Command to connect 
to the instance.

![Sin titulo](static/images/connect.png) 

Go to the terminal

`ssh -i "ubuntu1604.pem" ubuntu@ec2-3-135-212-26.us-east-2.compute.amazonaws.com`

If you get a Key-Pair permission error, change the permissions of the Key to 400 using the following command and retry 
the above command.

`chmod 400 ubuntu1604.pem` 

**Links**
- [Getting Started with Amazon EC2 Linux Instances](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EC2_GetStarted.html)
- [Step 2: Create Your EC2 Resources and Launch Your EC2 Instance](https://docs.aws.amazon.com/efs/latest/ug/gs-step-one-create-ec2-resources.html)
- [Elastic IP Addresses](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html)
- [Connect Using EC2 Instance Connect](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-connect-methods.html)
