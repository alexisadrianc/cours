# Docker, de principiante a experto, Udemy

 El primer recurso dado en el curso ya lo tenia, que es la actualización e 
 instalación del repositorio y docker respectivamente. (Ver Links 1 y 2)
 
 Docker Imagenes
 ------------------
  
 Las imagenes se almacenan en [Docker Hub](https://hub.docker.com/) con el 
 comando  `$ docker pull imagen` se podrá descargar cualquier imagen para 
 hacer el contenedor. Si al ejecutar el comando anterior no se le define 
 versión de la imagen el va a descargar la última `latest`. Las versiones 
 se toman del hub de docker 
 
   Ejemplos:
    
    $ docker pull odoo:12.0       
    $ docker pull odoo
 
 - Contrucción de imágenes personalizadas
  
    A partir de la creación de un fichero Dockerfile y ejecutando 
    `$ docker build --tag nombre_de_imagen_resultadoo .` o 
    `$ docker build -t nombre_de_la_imagen_resultado .` el punto indica 
    que estamos parado en la misma dirección que el Dockerfile
    
    Algunos comando para estudiar 
    
     - `docker ps `          Lista contenedores en ejecusión
     - `docker ps -a`        Lista contenedores que estan detenidos
     - `docker rm -fv contenedor` Elimina contenedor.
     - `docker rmi -f imagen` Elimina la imagen.
     - `docker images` Lista todas las imagenes
     - `docker images | grep imagen` Lista las imágenes que se llamen así.     
     - `docker history -H imagen` Mustra el historial de capas que ha tenido 
     la imagen.
     - `docker run -d --name nombre_contenedor -p XX:XX imagen` Ejecuta 
     el contenedor `-p XX:XX` es opcional 
     
     
  - DockerFile
  
    El siguiente es un Dockerfile sencillo para la instalación del OS Ubuntu 
    V:16.04 y un servidor Apache que siempre se va a ejecutar en primer plano. 
  
        FROM ubuntu:16.04 
  
        RUN apt-get install -y apt-get install apache2 -y && apt-get clean
    
        CMD apachectl -D FOREGROUND
        
    - `FROM`: Se le define el Sistema Operativo
    - `LABEL`: Etiquetas para agregar metadata al Dockerfile
    - `RUN`: 
    - `WORKDIR`:
    - `USER`: Define el usuario que esta ejecutando la tarea 
    - `COPY\ADD`: Copia archivos de nuestra PC a la imagen.
    - `ENV`: Para declarar variables de entorno
    - `CMD`: La instruccion que inicia el contenedor automaticamente
 
 - Images
    
    Las imagenes son capas de solo lectura 
    
    Eliminar imagenes sin nombre y sin tag 
    
    - Filtrar por atributos  `docker images -f dangling=true`
    - Mostrar solo el ID de las images `docker images -f dangling=true -q` 
    - Utilizar xargs para eliminar  `docker images -f dangling=true -q | xargs docker rmi`
    
 - Contenedores
    
    Los contenedores son una instancia de ejecusión de una imagen, ademas son temporales, tienen instancia una capa de 
    lectura y escritura y podemos crear a partir de una imagen n contenedores    
    
    - Inspeccionar caracteristicas del contenedor `docker inspect CONTAINER_NAME`
    - Descargar imagenes oficiales `docker pull imagen`
    - Correr el contenedor en segundo plano `docker run -d ...`
    - Mapear contenedor puerto de la PC XXXX: puerto del contenedor YYYYY `docker run -d p XXXX:YYYY ...`
    - Renombrar contenedor `docker rename old_name new_new`
    - Detener contendor `docker stop container or ID`
    - Iniciar contenedor `docker start container or ID`
    - Reiniciar contenedor `docker restart container or ID`
    - Ingresar al contenedor `docker exec -ti container or ID bash`
    - Ingresar al contenedor con user root `docker exec -u root -ti container or ID bash`
    - Listar el ID de los contenedores `docker ps -q`
    - Eliminar todos los contenedores `docker ps -q | xargs docker rm -f`
    - Crear variable de entorno en contenedor `docker run -d -e "VARIABLE=VALOR" ...`
    - Comprobar propiedades de un contenedor `docker stats CONTAINER`
    - Limitar recursos del contenedor: `docker run -d -m "CAPACIDAD DE RAM" --name ....`
    - Copiar a un contenedor `docker cp FILE CONTAINER:/tmp`
    - Copiar del contenedor a la pc `docker cp CONTAINER:RUTA_DEL_ARCHIVO_A_COPIAR RUTA_EN_PC`
    - Devuelve el ultimo contenedor creado `docker ps -l`
    
 - Volumenes
    
    Los volumenes permiten mantener la data persistente del contenedor cuando estos mueren. Existen tres tipo de 
    volumenes:
    - Host (Recomenado)
    - Anonymus (No recomendado)
    - Name Volumes (Recomendado)
    
    Volumenes de host: 
    
    Con los volmenes de host se logran guardar en la pc las carpetas importantes del contenedor, para que cuando 
    se elimine el contenedor se puedan tener persistente esta info y luego se pueda restaurar en otro container, 
    se utiliza la variable -v :
    
        `docker run -d --name NOMBRE -p XXXX:YYYY -e "VARIABLE=VALOR" -v UBICACION_PC:UBICACION_CONTAINER ...`
        
    Dangling Volume
    
    - Filtrar por atributos  `docker volume -f dangling=true`
    - Mostrar solo el ID de los volumenes `docker volume -f dangling=true -q` 
    - Utilizar xargs para eliminar  `docker volume -f dangling=true -q | xargs docker volume rm`
    
    Compartir volumenes entre contenedores
    
    Por lo general tenemos un volumen que genera data y los deja en un archivo y tenemos otro que los utiliza
    - Creamos el directorio donde se va a compartir el archivo
    
    Para crear un volumen `docker volume create NOMBREVOLUME`
    
 - Redes
    
    - Red por defecto de la interfaz docker `ip a | grep docker`
    - Red por defecto de los contenedores `docker network ls`    
    - Inspecciona como esta conformada la red (`bridge` Nombre de la red por defecto) `docker network inspect bridge`
    - hacer ping entre container `docker exec CONTAINER bash -c "ping IP_OTHER_CONTAINER"`
    - Crear una red en Docker `docker network create NAME`
    - Conectar docker en diferentes redes `docker network connect RED1 RED2`
    - Desconectar relacion de redes de Docker `docker network disconnect RED1 RED2`
    - Eliminar una red `docker network rm NAME`
    - Buscar si existe una red `docker network ls | grep NAME`
    - Asignarle un IP al un contenedor ``
    
    Ej: 
    - Crear red `docker network cerate --subnet XXX.XXX.XX.XXX/XX --gateway XXX.XXX.XX.X -d bridge NAME_RED`            
    - Crear contenedor en la red creada con IP dinamico `docker run --network NAME_RED -d --name CONTAINER -ti IMAGE`
    - Crear contenedor en la red creada con IP designado `docker run --network NAME_RED --ip XX.XXX.XXX.XX -d --name CONTAINER -ti IMAGE`
    
 - Docker Compose
   
   - Instalación en linux
   
     1. Ejecutar la terminal y escribir el comando `sudo su`
     2. Copiar del sitio de docker y pegar en la terminal la siguiente línea 
        
        `curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose`
     3.  Salir de sudo y darle permiso de ejecusión 
     
           `chmod +x /usr/local/bin/docker-compose`
   - Elementos del fichero docker-compose
     
        *Se puede nombrar el fichero con cualquier nombre pero con extension .yml y ejecutar con el siguiente comando -f
       `docker-compose -f NUEVO_FICHERO.yml up -d`*
     
           version: -> obligatorio
           services: -> obligatorio
           volumes: -> opcional
           network: -> opcional
   
        - Documentación para docker compose: [`https://docs.docker.com/compose/compose-file/`](https://docs.docker.com/compose/compose-file/)  
        - Ejecutar docker compose: Desde la ubicación del archivo ejecutar `docker-compose up -d`
        - Eliminar el contenedor con docker-compose: Desde la ubicación del archivo ejecutar  `docker-compose down`
        - Costruir imagen desde docker-compose
            - Tener el fichero Dockerfile en el mismo directorio que el docker-compose
            - en el fichero docker-compose
              - build . es para que busque el Dockerfile de la misma ubicación
          
                     version: '3'
                     services:
                         container_name: web
                         image:  nginx
                         build: .
            - Para que llame a un Dockerfile con otro nombre q no sea el q trae por defecto `Dockerfile`
              en context definimos la ruta del fichero y dockerfile es para definir el nombre del dockerfile 
          
                    version: '3'
                    services:
                        web:
                        container_name: web
                        image:  nginx
                        build:
                            context: .
                            dockerfile: NAME_DOCKERFILE
       
   - Limitar contenedor con docker-compose  con `mem_limit` and `cpuset`    
    
            version: '3'
            services:
            web:
                container_name: web
                mem_limit:  20m
                cpuset: "0"
                image:  nginx:alpine
   - Política de reinicio de contenedor
    
        1. `no` - *(default) Nunca lo reinicia autmáticamente*
        2. `always` - *Siempre reinicialo*
        3. `unless-stopped` - *Reinicialo si lo detuve*
        4. `on-failure` - *Reinicialo si lanzo una falla*
    
                version: '3'
                services:
                    web:
                        container_name: web
                        image:  nginx
                        build: .
                        restart: always
        Tambien se puede reiniciar un contenedor cuando se ejecuta el docker run
        
        `docker run -dti --restart CONDICION NAME_CONTAINER`
    
Test:

La idea de este articulo es que le des solución al siguiente problema utilizando lo que has aprendido.

Deberás crear un dockre-compose para cada definición de contenedores de los ejercicios anteriores:

Puntos a tener en cuenta:

    * Docker Compose V2

    * La imagen (De apache y php) debe poder construirse desde el docker-compose

    * Se deben definir los volúmenes solicitados en el ejericicio anterior, más las variables de entorno, límite y demás requisitos especificados en los ejercicios anteriores.

---

Adicional a esto, te piden:

Crear un docker-compose v3 con dos servicios:

db y admin.

En el servicio DB, debe ir una db con mysql:5.7 y las credenciales de tu preferencia.

En el admin, debes usar la imagen oficial de phpmyadmin, y por medio de redes, comunicarla con mysql. Debes exponer el puerto de tu preferencia y para validar que funcione, debes loguearte en el UI de phpmyadmin vía navegador, usando las credenciales del root de mysql.

Que te diviertas!
    
     
        
 
    
   
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

 
 
 
 
 
 **Link**
 
  - [Docker](https://gitlab.com/alexisadrianc/cours/-/tree/master/Docker)
  - [Install.txt Cursos. Udemy](https://www.udemy.com/course/docker-de-principiante-a-experto/learn/lecture/10696218#notes)
  - [Docker Compose Documentation](https://docs.docker.com/compose/compose-file/)  
  - [Todos los recursos, archivos, Dockerfile. Udemy](https://github.com/ricardoandre97/docker-es-resources)