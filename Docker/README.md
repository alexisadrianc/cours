# Docker 

![Sin titulo](static/images/docker.png) 

Docker is an open source project that automates the deployment of
applications inside software containers, providing an additional layer
of abstraction and automation of virtualization of applications in multiple
operating systems.

Uninstall old versions
----------------------------

Older versions of Docker were called `docker` or `docker-engine`. If these are 
installed, uninstall them:

 `$ sudo apt-get remove docker docker-engine docker.io`
        
Install Docker with Ubuntu 
---------------------------------

1. Update the apt package index:

    `$ sudo apt-get update`
    
2. Install packages to allow `apt` to use a repository over HTTPS:

```    
    $ sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common
```        

3. Add Docker’s official GPG key:

    `$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -`
    
    Verify that you now have the key with the fingerprint `9DC8 5822 9FC7 DD38 854A E2D8 8D81 803C 0EBF CD88`, 
    by searching for the last 8 characters of the fingerprint.
    
    `$ sudo apt-key fingerprint 0EBFCD88`
    
    **Answers**
    
    ```
    pub   4096R/0EBFCD88 2017-02-22
            Key fingerprint = 9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
    uid                  Docker Release (CE deb) <docker@docker.com>
    sub   4096R/F273FCD8 2017-02-22
    ```

4. Use the following command to set up the **stable** repository. You always need 
the **stable** repository, even if you want to install builds from the **edge** or **test** 
repositories as well. To add the **edge** or **test** repository, add the word `edge` or 
`test` (or both) after the word `stable` in the commands below.

    > Note: The `lsb_release -cs` sub-command below returns the name of your 
    Ubuntu distribution, such as `xenial`. Sometimes, in a distribution like 
    Linux Mint, you might have to change `$(lsb_release -cs)` to your parent 
    Ubuntu distribution. For example, if you are using `Linux Mint Rafaela`, 
    you could use `trusty`.
    
```    
    $ sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"
```

INSTALL DOCKER CE
---------------------

1. Update the `apt` package index.

    `$ sudo apt-get update`
   
2. Install the latest version of Docker CE, or go to the next step to install a specific version. Any existing 
installation of Docker is replaced.

    `$ sudo apt-get install docker-ce`

Uninstall Docker CE
-----------------------

1. Uninstall the Docker CE package:

    `$ sudo apt-get purge docker-ce`
    
2. Images, containers, volumes, or customized configuration files on your host are not automatically removed. 
To delete all images, containers, and volumes:

    `$ sudo rm -rf /var/lib/docker`
    
You must delete any edited configuration files manually.

**Links**

- [Docker for Linux](https://docs.docker.com/install/linux/docker-ce/ubuntu/)