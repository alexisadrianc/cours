# Odoo inside of docker

![Sin titulo](static/images/logo.png) 

Start a PostgreSQL server
--------------------------

`$ docker run -d -e POSTGRES_USER=odoo -e POSTGRES_PASSWORD=odoo -e POSTGRES_DB=postgres --name db postgres:10`

Start an Odoo instance
-----------------------

`$ docker run -p 8069:8069 --name odoo --link db:db -t odoo:12`

The alias of the container running Postgres must be db for Odoo to be able to connect to the Postgres server.

Stop and restart an Odoo instance
----------------------------------
```
$ docker stop odoo
$ docker start -a odoo
```

Environment Variables
------------------------
Tweak these environment variables to easily connect to a postgres server:

- `HOST`: The address of the postgres server. If you used a postgres container, set to the name of the container. Defaults to `db`.
- `PORT`: The port the postgres server is listening to. Defaults to `5432`.
- `USER`: The postgres role with which Odoo will connect. If you used a postgres container, set to the same value as `POSTGRES_USER`. Defaults to `odoo`.
- `PASSWORD`: The password of the postgres role with which Odoo will connect. If you used a postgres container, set to the same value as `POSTGRES_PASSWORD`. Defaults to `odoo`.

Docker Compose examples
--------------------------

The simplest `docker-compose.yml` file would be:

Here's a last example showing you how to mount custom addons, how to use a custom configuration file and how to use 
volumes for the Odoo and postgres data dir:

```
version: '2'
services:
  web:
    image: odoo:12.0
    depends_on:
      - db
    ports:
      - "8069:8069"
    volumes:
      - odoo-web-data:/var/lib/odoo
      - ./config:/etc/odoo
      - ./addons:/mnt/extra-addons
  db:
    image: postgres:10
    environment:
      - POSTGRES_DB=postgres
      - POSTGRES_PASSWORD=odoo
      - POSTGRES_USER=odoo
      - PGDATA=/var/lib/postgresql/data/pgdata
    volumes:
      - odoo-db-data:/var/lib/postgresql/data/pgdata
volumes:
  odoo-web-data:
  odoo-db-data:
``` 
 
To start your Odoo instance, go in the directory of the `docker-compose.yml` file you created from the previous examples 
and type:

`docker-compose up -d`

**Links**

- [odoo - Docker Hub](https://hub.docker.com/_/odoo)