# Domain for Odoo in Amazon Web Service (AWS)

*[Step 1 - Launch Instance](https://gitlab.com/alexisadrianc/cours/-/tree/master/AWS)*

*[Step 2 - Install Docker](https://gitlab.com/alexisadrianc/cours/-/tree/master/Docker)*

*[Step 3 - Install Odoo inside to Docker](https://gitlab.com/alexisadrianc/cours/-/tree/master/odoo%20inside%20to%20docker)*

*Step 4 Domain with Route53 in AWS*

### Domain registration pricing
For information about the cost to register domains, see [Amazon Route 53 Pricing for Domain Registration](https://d32ze2gidvkk54.cloudfront.net/Amazon_Route_53_Domain_Registration_Pricing_20140731.pdf).

#### To register a new domain using Route 53

1. Sign in to the AWS Management Console and open the Route 53 console at https://console.aws.amazon.com/route53/.

2. If you're new to Route 53, under Domain Registration, choose Get Started Now.

    If you're already using Route 53, in the navigation pane, choose Registered Domains.

3. Choose Register Domain, and specify the domain that you want to register:

**Links**

- [Curso AWS - configuación DNS con Route 53](https://youtu.be/cBPdYWga1Vg)
- [Configuring Amazon Route 53 as Your DNS Service](https://docs.aws.amazon.com/es_es/Route53/latest/DeveloperGuide/dns-configuring.html)

*Step 4 Install Nginx*

![Sin titulo](static/images/nginx.png) 

Install Nginx
--------------
Install Nginx using the followng command.

`sudo apt install nginx`

Remove default Nginx configurations.

```
sudo rm /etc/nginx/sites-enabled/default
sudo rm /etc/nginx/sites-available/default
```

### Configure Nginx Reverse proxy for Odoo

Create a new Nginx configuration for Odoo in the `sites-available` directory.

`sudo nano /etc/nginx/sites-available/odoo.conf`

Copy and paste the following configuration, ensure that you change the server_name to match your domain name.

```
upstream odooserver {
    server 127.0.0.1:8069;
}

upstream odoochat {
    server 127.0.0.1:8072;
}

server {
     listen 80;
     server_name mydomain.com www.mydomain.com;

     access_log /var/log/nginx/odoo_access.log;
     error_log /var/log/nginx/odoo_error.log;

     proxy_read_timeout 720s;
     proxy_connect_timeout 720s;
     proxy_send_timeout 720s;
     proxy_set_header X-Forwarded-Host $host;
     proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
     proxy_set_header X-Forwarded-Proto $scheme;
     proxy_set_header X-Real-IP $remote_addr;

     location / {
        proxy_redirect off;
        proxy_pass http://odooserver;
     }

     location ~* /web/static/ {
         proxy_cache_valid 200 90m;
         proxy_buffering on;
         expires 864000;
         proxy_pass http://odooserver;
     }

     gzip_types text/css text/less text/plain text/xml application/xml application/json application/javascript;
     gzip on;
 }
```

Hit `Ctrl+X` followed by `Y` and Enter to save the file and exit.

To enable this newly created website configuration, symlink the file that you just created into the `sites-enabled` directory.

`sudo ln -s /etc/nginx/sites-available/odoo.conf /etc/nginx/sites-enabled/odoo.conf`

Restart Nginx for the changes to take effect.

`sudo systemctl restart nginx`

### Install and configure SSL for Odoo
#### HTTPS
HTTPS is a protocol for secure communication between a server (instance) and a client (web browser). Due to the 
introduction of Let’s Encrypt, which provides free SSL certificates, HTTPS are adopted by everyone and also provides 
trust to your audiences.

```
sudo add-apt-repository ppa:certbot/certbot
sudo apt update
sudo apt install python-certbot-nginx
```

Now we have installed Certbot by Let’s Encrypt for Ubuntu 18.04, run this command to receive your certificates.

`sudo certbot --nginx -d mydomain.com -d www.mydomain.com`

Enter your `email` and agree to the terms and conditions, then you will receive the list of domains you need to generate 
SSL certificate.

To select all domains simply hit `Enter`

The Certbot client will automatically generate the new certificate for your domain. Now we need to update the Nginx 
config.

Check your configuration and restart Nginx for the changes to take effect.
```
sudo nginx -t
sudo service nginx restart
```